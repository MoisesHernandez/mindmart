import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  // { id: , name: '', price: '', category: '',  imageURL: ''}
  private data = [
    {
      department: 'Despensa',
      expanded: true,
      imageURL: '',
      category: ['Café, Té y Sustitutos', 'Pan y Tortillas Empacados'],
      products: [
        { id: 0, name: 'Café molido Garat americano regular 1 kg', price: '199', category: 'Café, Té y Sustitutos', imageURL: 'https://res.cloudinary.com/walmart-labs/image/upload/w_960,dpr_auto,f_auto,q_auto:best/gr/images/product-images/img_large/00750105241902L.jpg'},
        { id: 1, name: 'Café soluble Nescafé clásico 400 g', price: '152', category: 'Café, Té y Sustitutos', imageURL: 'https://res.cloudinary.com/walmart-labs/image/upload/w_960,dpr_auto,f_auto,q_auto:best/gr/images/product-images/img_large/00750105862020L.jpg'},
        { id: 2, name: 'Pan Bimbo cero cero 567 g', price: '43', category: 'Pan y Tortillas Empacados',  imageURL: 'https://res.cloudinary.com/walmart-labs/image/upload/w_960,dpr_auto,f_auto,q_auto:best/gr/images/product-images/img_large/00750103046709L.jpg'},
        { id: 3, name: 'Pan blanco Bimbo extra grande 740 g', price: '41', category: 'Pan y Tortillas Empacados',  imageURL: 'https://res.cloudinary.com/walmart-labs/image/upload/w_960,dpr_auto,f_auto,q_auto:best/gr/images/product-images/img_large/00750100011112L.jpg'}
      ]
    },
    {
      department: 'Lácteos',
      imageURL: '',
      category: ['Leche','Yogurt'],
      products: [
        { id: 5, name: 'Leche Lala deslactosada caja con 6 pzas de 1 l c/u', price: '105', category: 'Leche',  imageURL: 'https://res.cloudinary.com/walmart-labs/image/upload/w_960,dpr_auto,f_auto,q_auto:best/gr/images/product-images/img_large/00750102055817L.jpg'},
        { id: 6, name: 'Leche Lala deslactosada light baja en grasa caja con 6 pzas de 1 l c/u', price: '105', category: 'Leche',  imageURL: 'https://res.cloudinary.com/walmart-labs/image/upload/w_960,dpr_auto,f_auto,q_auto:best/gr/images/product-images/img_large/00750102055818L.jpg'},
        { id: 7, name: 'Yakult 5 pzas de 80 ml c/u', price: '31', category: 'Yogurt',  imageURL: 'https://res.cloudinary.com/walmart-labs/image/upload/w_960,dpr_auto,f_auto,q_auto:best/gr/images/product-images/img_large/00750102551100L.jpg'},
        { id: 8, name: 'Danone Danonino lunch fresa 70 g', price: '10', category: 'Yogurt',  imageURL: 'https://res.cloudinary.com/walmart-labs/image/upload/w_960,dpr_auto,f_auto,q_auto:best/gr/images/product-images/img_large/00750103239784L.jpg'}
      ]
    },
    {
      department: 'Frutas y Verduras',
      imageURL: '',
      category: ['Frutas','Verduras'],
      products: [
        { id: 9, name: 'Plátano Chiapas por kg', price: '21', category: 'Frutas',  imageURL: 'https://res.cloudinary.com/walmart-labs/image/upload/w_960,dpr_auto,f_auto,q_auto:best/gr/images/product-images/img_large/00000000004011L.jpg'},
        { id: 10, name: 'Papaya maradol por kilo', price: '30', category: 'Frutas',  imageURL: 'https://res.cloudinary.com/walmart-labs/image/upload/w_960,dpr_auto,f_auto,q_auto:best/gr/images/product-images/img_large/00000000003112L.jpg'},
        { id: 11, name: 'Jitomate saladet por kilo', price: '37', category: 'Verduras',  imageURL: 'https://res.cloudinary.com/walmart-labs/image/upload/w_960,dpr_auto,f_auto,q_auto:best/gr/images/product-images/img_large/00000000004087L.jpg'},
        { id: 12, name: 'Cebolla blanca por kilo', price: '25', category: 'Verduras',  imageURL: 'https://res.cloudinary.com/walmart-labs/image/upload/w_960,dpr_auto,f_auto,q_auto:best/gr/images/product-images/img_large/00000000004663L.jpg'}
      ]
     }
  ];
 
  private cart = [];
 
  constructor() { }
 
  getProducts() {
    return this.data;
  }

  getDepartment(departmentName: string){
    return {
      ...this.data.find(deparment => {
        return deparment.department === departmentName
      })
    }
  }
 
  getCart() {
    return this.cart;
  }
 
  addProduct(product) {
    this.cart.push(product);
  }
}
