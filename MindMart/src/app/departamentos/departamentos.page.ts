import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { CartService } from '../cart.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-departamentos',
  templateUrl: './departamentos.page.html',
  styleUrls: ['./departamentos.page.scss'],
})
export class DepartamentosPage implements OnInit {

  cart = [];
  items = [];
  
  constructor(private router: Router, private cartService: CartService, private navCtrl: NavController) { }

  ngOnInit() {
    this.items = this.cartService.getProducts();
    this.cart = this.cartService.getCart();
  }

  addToCart(product) {
    this.cartService.addProduct(product);
  }

  openCart() {
    this.router.navigate(['cart']);
  }

  openDetailDepartment(departamentQuery: string){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(departamentQuery)
      }
    };
    this.router.navigate(['departamento'], navigationExtras);
  }

}
