import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-departamento',
  templateUrl: './departamento.page.html',
  styleUrls: ['./departamento.page.scss'],
})
export class DepartamentoPage implements OnInit {

  data: any;
 
  constructor(private route: ActivatedRoute, private router: Router, private cartService: CartService) {
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        this.data = JSON.parse(params.special);
      }
    });
  }

  cart = [];
  items = [];
  deparment = {
    department: '',
      imageURL: '',
      category: [],
      products: []
  };

  ngOnInit() {
    this.cart = this.cartService.getCart();
    this.data =this.data.replace(/_/g," ");
    this.deparment = this.cartService.getDepartment(this.data);
    console.log(this.deparment);
    let arrayAux = [];
    for(let i = 0; i < this.deparment.category.length; i++)
    {
      let objectAux = {
        subdeparment: '',
        products: []
      };
      let subDepartment = this.deparment.category[i];
      objectAux.subdeparment = subDepartment;
      this.deparment.products.forEach(function (elemento, indice, array) {
        if(elemento.category === subDepartment)
        {
          arrayAux.push(elemento);
        }
        
      });
      objectAux.products = arrayAux;
      arrayAux = [];
      this.items.push(objectAux);
    }
    console.log(this.items);
  }

  addToCart(product) {
    this.cartService.addProduct(product);
  }

  openCart() {
    this.router.navigate(['cart']);
  }

}
